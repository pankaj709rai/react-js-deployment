var HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack')
module.exports = env=>{
    
  return{  mode: env.mode,
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                loaders: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    plugins: [
        new Dotenv({
            path:env.mode==='production'? './.env.production':'./.env.development',
            encoding: 'utf8',
        }),
        new HtmlWebpackPlugin({
        template: './src/index.html'
    })],
    
    devServer: {
        historyApiFallback: true
    },
    externals: {
        // global app config object
        config: JSON.stringify({
            apiUrl: 'http://localhost:3000'
        })
    }
}}